<?php

/**
 * @file
 *
 * Parish info configuration form.
 */

/**
 * Parish information configuration form.
 */
function parish_info_configuration_form() {
  $form['description'] = array(
    '#markup' => '<p>' . t('You can manage settings for parish information on this page.') . '</p>',
  );

  $form['parish_info_parish_website'] = array(
    '#type' => 'checkbox',
    '#title' => t('This is a parish website.'),
    '#default_value' => variable_get('parish_info_parish_website', 1),
  );

  $form['parish_bulk_info'] = array(
    '#type' => 'item',
    '#markup' => t("There is currently no interface for adding multiple parishes (for, as an example, a diocesan website). This feature is coming soon."),
    '#states' => array(
      'invisible' => array(
        'input[name="parish_info_parish_website"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['parish_info'] = array(
    '#type' => 'fieldset',
    '#title' => t("Information for !site", array('!site' => variable_get('site_name'))),
    '#tree' => TRUE,
    '#states' => array(
      'invisible' => array(
        'input[name="parish_info_parish_website"]' => array('checked' => FALSE),
      ),
    ),
  );

  // Get this parish's information.
  $parish_info = parish_info_get_info();

  $form['parish_info']['description'] = array(
    '#markup' => '<p>' . t("Please enter your parish's information below and click 'Save Configuration' when finished.") . '</p>',
  );
  $form['parish_info']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Parish Name'),
    '#size' => 55,
    '#maxlength' => 255,
    '#default_value' => $parish_info['name'],
    '#required' => TRUE,
  );
  $form['parish_info']['address_street'] = array(
    '#type' => 'textfield',
    '#title' => t('Street Address'),
    '#size' => 45,
    '#maxlength' => 255,
    '#default_value' => $parish_info['address_street'],
    '#required' => TRUE,
  );
  $form['parish_info']['address_street_2'] = array(
    '#type' => 'textfield',
    '#title' => t('Street Address 2'),
    '#size' => 45,
    '#maxlength' => 255,
    '#default_value' => $parish_info['address_street_2'],
  );
  $form['parish_info']['address_city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => $parish_info['address_city'],
    '#required' => TRUE,
  );
  $form['parish_info']['address_state'] = array(
    '#type' => 'select',
    '#title' => t('State'),
    '#options' => _parish_info_list_us_states(),
    '#default_value' => $parish_info['address_state'],
    '#required' => TRUE,
  );
  $form['parish_info']['address_zip'] = array(
    '#type' => 'textfield',
    '#title' => t('Zip Code'),
    '#size' => 10,
    '#maxlength' => 10,
    '#element_validate' => array('_parish_info_validate_zip'),
    '#default_value' => $parish_info['address_zip'],
    '#required' => TRUE,
  );
  $form['parish_info']['phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone number'),
    '#size' => 15,
    '#maxlength' => 15,
    '#element_validate' => array('_parish_info_validate_phone'),
    '#default_value' => $parish_info['phone'],
  );
  $form['parish_info']['fax'] = array(
    '#type' => 'textfield',
    '#title' => t('Fax number'),
    '#size' => 15,
    '#maxlength' => 15,
    '#element_validate' => array('_parish_info_validate_phone'),
    '#default_value' => $parish_info['fax'],
  );
  $form['parish_info']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Parish Email'),
    '#size' => 35,
    '#maxlength' => 255,
    '#element_validate' => array('_parish_info_validate_email'),
    '#default_value' => $parish_info['email'],
  );
  $form['parish_info']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Parish Email'),
    '#size' => 35,
    '#maxlength' => 255,
    '#element_validate' => array('_parish_info_validate_email'),
    '#default_value' => $parish_info['email'],
  );

  $form['#submit'][] = 'parish_info_configuration_form_submit';

  return system_settings_form($form);
}

function parish_info_configuration_form_submit($form, &$form_state) {
  // If there is information entered for this parish, update the data in the db.
  if ($form_state['values']['parish_info_parish_website']) {
    // Get the parish info values.
    $parish_info = $form_state['values']['parish_info'];
    // Set the 'this_site' variable for this site.
    $parish_info['this_site'] = 1;

    // Use db_merge() to either update or insert the row for this site.
    db_merge('parish_info')
      ->key(array('this_site' => $parish_info['this_site']))
      ->fields($parish_info)
      ->execute();
  }
}

/**
 * Returns an array of US States, with 2-digit abbreviations as keys.
 */
function _parish_info_list_us_states() {
  return array(
    'AL' => 'Alabama',
    'AK' => 'Alaska',
    'AZ' => 'Arizona',
    'AR' => 'Arkansas',
    'CA' => 'California',
    'CO' => 'Colorado',
    'CT' => 'Connecticut',
    'DE' => 'Delaware',
    'DC' => 'District Of Columbia',
    'FL' => 'Florida',
    'GA' => 'Georgia',
    'HI' => 'Hawaii',
    'ID' => 'Idaho',
    'IL' => 'Illinois',
    'IN' => 'Indiana',
    'IA' => 'Iowa',
    'KS' => 'Kansas',
    'KY' => 'Kentucky',
    'LA' => 'Louisiana',
    'ME' => 'Maine',
    'MD' => 'Maryland',
    'MA' => 'Massachusetts',
    'MI' => 'Michigan',
    'MN' => 'Minnesota',
    'MS' => 'Mississippi',
    'MO' => 'Missouri',
    'MT' => 'Montana',
    'NE' => 'Nebraska',
    'NV' => 'Nevada',
    'NH' => 'New Hampshire',
    'NJ' => 'New Jersey',
    'NM' => 'New Mexico',
    'NY' => 'New York',
    'NC' => 'North Carolina',
    'ND' => 'North Dakota',
    'OH' => 'Ohio',
    'OK' => 'Oklahoma',
    'OR' => 'Oregon',
    'PA' => 'Pennsylvania',
    'RI' => 'Rhode Island',
    'SC' => 'South Carolina',
    'SD' => 'South Dakota',
    'TN' => 'Tennessee',
    'TX' => 'Texas',
    'UT' => 'Utah',
    'VT' => 'Vermont',
    'VA' => 'Virginia',
    'WA' => 'Washington',
    'WV' => 'West Virginia',
    'WI' => 'Wisconsin',
    'WY' => 'Wyoming',
  );
}
