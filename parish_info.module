<?php

/**
 * @file parish_info.module
 *
 * Parish information management module.
 */

/******************************************************************************
* CORE HOOKS
******************************************************************************/

/**
 * Implements hook_permission().
 */
function parish_info_permission() {
  return array(
    'administer parish info' => array(
      'title' => t('Administer Parish Info'),
      'description' => t('Change parish info settings.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function parish_info_menu() {
  $items['admin/config/system/parish_info'] = array(
    'title' => 'Parish Info',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('parish_info_configuration_form'),
    'access callback' => 'user_access',
    'access arguments' => array('administer parish info'),
    'file' => 'includes/parish_info.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/******************************************************************************
* HELPER FUNCTIONS
******************************************************************************/

/**
 * Get a parish's info by id number.
 *
 * @param $id
 *   If ID is not given, the row for 'this_site' will be returned.
 */
function parish_info_get_info($id = NULL) {
  if ($id) {
    return db_query("SELECT * FROM {parish_info} WHERE id = :id", array(':id' => $id))->fetchAssoc();
  }
  else {
    return db_query("SELECT * FROM {parish_info} WHERE this_site = 1")->fetchAssoc();
  }
}

/**
 * Validate a given email address.
 */
function _parish_info_validate_email($element, &$form_state, $form) {
  if (!empty($element['#value']) && !valid_email_address($element['#value'])) {
    form_error($element, t('Please enter a valid email address (Example: <em>parish@example.com</em>).'));
  }
}

/**
 * Validate a given zip code.
 */
function _parish_info_validate_zip($element, &$form_state, $form) {
  if (!empty($element['#value']) && !preg_match("/^([0-9]{5})(-[0-9]{4})?$/i", $element['#value'])) {
    form_error($element, t('Please enter a valid US zip code (Example: <em>63119</em> or <em>63119-0334</em>).'));
  }
}

/**
 * Validate a given phone number.
 */
function _parish_info_validate_phone($element, &$form_state, $form) {
  if (!empty($element['#value']) && !preg_match('/\(?\d{3}\)?[-\s.]?\d{3}[-\s.]\d{4}/x', $element['#value'])) {
    form_error($element, t('Please enter a valid US phone number (Example: <em>314-444-4444</em>).'));
  }
}
